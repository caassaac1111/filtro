/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject6;
import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author caas1
 */

public class filtro {

    private static Connection connection;

    public static void main(String[] args) {
        String jdbcUrl = "jdbc:mysql://localhost:3307/bd_personas";
        String username = "root";
        String password = "G#0avMLe36h$nWi";
        String csvFilePath = "resources/ciudadanos.csv";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/your_database", "root", "G#0avMLe36h$nWi");

            processCSV(csvFilePath);
        } catch (ClassNotFoundException | SQLException | ParseException e) {
            e.printStackTrace();
        } finally {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            }
        }
    } 

    private static void processCSV(String filePath) throws ParseException {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");

                if (!hasNumbers(data[1]) && !hasNumbers(data[2]) && !hasNumbers(data[3]) && !hasNumbers(data[4]) && !hasLetters(data[0])) {
                    String ci = data[0];
                    String names = concatenateNames(data[1], data[2], data[3]);
                    String apellidos = concatenateNames(data[4], "");

                    String fecha = convertDate(data[5]);

                    saveToDatabase(ci, names, apellidos, fecha);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean hasNumbers(String str) {
        return str.matches(".*\\d+.*");
    }

    private static boolean hasLetters(String str) {
        return str.matches(".*[a-zA-Z]+.*");
    }

    private static String concatenateNames(String... names) {
        StringBuilder result = new StringBuilder();
        for (String name : names) {
            result.append(name).append(" ");
        }
        return result.toString().trim();
    }

    private static String convertDate(String inputDate) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("dd/MM/yy");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = inputFormat.parse(inputDate);
        return outputFormat.format(date);
    }

    private static void saveToDatabase(String ci, String names, String apellidos, String fecha) throws SQLException {
        String sql = "INSERT INTO ciudadano (ci, nombres, apellidos, fecha) VALUES (?, ?, ?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, Integer.parseInt(ci));
        preparedStatement.setString(2, names);
        preparedStatement.setString(3, apellidos);
        preparedStatement.setString(4, fecha);
        preparedStatement.executeUpdate();
    }
}
